package main

import (
	"fmt"
	"runtime"
	"time"
)

func (a *App) chkError() {
	for msg := range a.Error {
		fmt.Println(msg)
	}
}

func MemStat() {
	t := time.NewTicker(time.Millisecond * 50)
	for {
		select {
		case <-t.C:
			memStats := &runtime.MemStats{}
			_ = make([]string, 1000000)

			runtime.ReadMemStats(memStats)
			fmt.Printf("\nAlloc = %v\nTotalAlloc = %v\nSys = %v\nNumGC = %v\n\n", memStats.Alloc/1024, memStats.TotalAlloc/1024, memStats.Sys/1024, memStats.NumGC)
		}
	}
}
