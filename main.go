package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	var a App
	a.Error = make(chan error)
	a.Exit = make(chan bool)

	//go MemStat()
	a.readSitesFromCsv()
	go a.chkError()
	go a.healthCheck()

	pos := 9999999

	t := time.NewTicker(time.Second * 1)
	tn := time.Now()
	for {
		select {
		case <-t.C:
			site := a.Sites[pos]
			if !site.LastCheck.IsZero() {
				fmt.Printf("pos: %d;\nsite: %s;\nstatus: %t;\n%v;\n\n", pos, site.Url, site.Status, site.LastCheck.Sub(tn))
				os.Exit(0)
			}
		}
	}
}

func (a *App) readSitesFromCsv() {
	csvFile, _ := os.Open("top10milliondomains.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))
	var ops uint64
	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			a.Error <- err
			continue
		}
		a.addSite(line[1])
		atomic.AddUint64(&ops, 1)
	}
}

func (a *App) addSite(site string) {
	a.Sites = append(a.Sites, &Site{Url: site})
}

func (s *Site) chkAlive(wg *sync.WaitGroup) {

	conn, err := net.DialTimeout("tcp", fmt.Sprintf("%s:80", s.Url), 2*time.Second)
	if err != nil {
		s.setStatus(false, err)
	} else {
		s.setStatus(true, err)
	}

	if conn != nil {
		_ = conn.Close()
	}
	wg.Done()
}
func (s *Site) printDomain() {
	log.Println(s.Url)
}

func (s *Site) setStatus(a bool, err error) {
	s.mux.Lock()
	if s.Status != a {
		s.Status = a
	}
	s.LastCheck = time.Now()
	s.DialTcpErr = err
	s.mux.Unlock()
}

func (a *App) updateCurrent() {
	a.current = atomic.AddUint64(&a.current, uint64(1)) % uint64(len(a.Sites))
}

func (a *App) healthCheck() {
	var wg sync.WaitGroup
	var ops uint64
	var packet []*Site
	packetLen := uint64(500)

	for {
		atomic.StoreUint64(&ops, atomic.AddUint64(&ops, uint64(1))%packetLen)

		packet = append(packet, a.Sites[a.current])
		a.updateCurrent()
		if ops == 0 {
			for _, s := range packet {
				wg.Add(1)
				go s.chkAlive(&wg)
			}
			wg.Wait()
			packet = []*Site{}
		}
	}
}
