package main

import (
	"sync"
	"time"
)

type App struct {
	Exit    chan bool
	Sites   []*Site
	Error   chan error
	current uint64
}

type Site struct {
	Url        string
	Status     bool
	mux        sync.Mutex
	DialTcpErr error
	LastCheck  time.Time
}
